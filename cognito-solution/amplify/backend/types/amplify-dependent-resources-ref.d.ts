export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "cognitoAuthSolution": {
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    }
}