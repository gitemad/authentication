import { Amplify } from "aws-amplify";

import { withAuthenticator } from "@aws-amplify/ui-react";
import "@aws-amplify/ui-react/styles.css";

import awsExports from "./aws-exports";
Amplify.configure(awsExports);

interface CognitoProps {
  user: {
    attributes: {
      email: string;
      family_name: string;
      name: string;
    };
    username: string; // uniqe-key
  };
  signOut: any;
}

function App({ signOut, user }: CognitoProps) {
  return (
    <>
      <h1>
        Welcome {user.attributes.name} {user.attributes.family_name}!
      </h1>
      <button title="signoutButton" onClick={signOut}>
        Sign out
      </button>
    </>
  );
}

export default withAuthenticator(App);
